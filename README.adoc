= Overlord component framework for Python
:toc: left

Homepage: https://gitlab.com/omaha-bgg/overlord

The Overlord framework is composed of several layers:

- a *plugin* system, allowing a class in an inversion-of-control way
  to accept externally-provided classes, as specified by a set of
  constraints.  This was designed for the Omaha board-game GUI, to let
  third-parties provide new games, new skins, new rendering system,
  new GUI toolkits, etc.

- a system to constraint designated class attributes, dubbed
  *parameters*, make it easy to let a user customize the parameter's
  value within the given constraints, and store any user's
  customization in a *preferences* storage.  Provided constraints
  include eg. integer ranges, single and multiple selection of an item
  from a given list, font and color selection, and plugin selection.

- an *inner-class* type-constraining mechanism, to ensure that derived
  classes don't break the Liskov Substitution Principle.


== Copyright and license

Overlord is Copyright (c) 2009-2023 by Yann Dirson <ydirson@free.fr>.

This program and its libraries are free software; you can
redistribute them and/or modify it under the terms of the GNU
Lesser General Public License as published by the Free Software
Foundation; either version 2.1 of the License.

This program and these libraries are distributed in the hope that
it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free
Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA


== Dependencies

To install Overlord from git source, you will need;

- setuptools
- setuptools_scm
- python 3.9 or newer

To use Overlord, currently you will need at least:

- pyxdg (tested with v0.19)
   v0.15 (Debian lenny) seems to work once the DesktopEntry.__cmp__ method
   has been deleted from xdg/DesktopEntry.py (causes infinite recursion).

To run the unit tests:

- nose2

To run static analysis:

- pylint 2.x (tested with 2.7.2)


== Running tests

=== Unit tests

Run "nose2-3" from the shell to run all tests expected to pass.


== API overview

Note that this description is still partial, and the architecture
itself is still a moving target.  Use "git blame" on this section to
identify the version of the code that it is supposed to describe.

=== Parameterization

Overlord provides a way to complement the Python mechanism of object
attributes, with enforcement of validity rules.  Example of such
constraints are integer or floating-point number ranges, and choices
from a predefined list; to support those, the basic constraint is
enforcement of a given object type.  Those attributes under Overlord
control are named *parameters*.

Classes whose instances take parameters must be subclasses of
Overlord.Parametrizable.

==== Parametrizable context

Some parameters may depend on other parameters' values.  Eg. the
"PieceRenderer" parameter in a GameRenderer subclass is not the same
for Chess and for Go: they do not allow to select from the same set of
choices, and must be stored separately in the preferences.  So a given
parameter (Overlord.Param) is constrainted not only by its parameter
declaration, but also by the context in which it gets instanciated.

This is expressed by a Context object, which contains a dict mapping a
parameter key to Param instances.  The parameter key to be used when
adding a given parametrizable class to a context is defined by
toplevel classes.  Prominent examples of such classes, which do define
the `context_key` class method, are Omaha.Core.Game and Omaha.Core.PieceHolder;
this formalizes that choice of eg. different themes can be chosen
respectively for different types of game, and that a game's main board
generally gets rendered differently from eg. a player's hand or captures.

==== Declaring parameters

There are two things to add to a class definition to let its instances
have a parameter: a declaration of the parameter's type and
constraints (by creating an object of a type-specific class derived
from `Overlord.params.ParamDecl`), and a optionally some
parameter-setting code to react to the object's parameter value being
modified.

The easiest way to declare a parameter is to create a method decorated
with `@Overlord.parameter`, which should return an instance of a class
derived from `Overlord.params.ParamDecl`.  Overlord itself provides
such classes for simple parameter types (String, Int, Float), for making a
choice from a list of options (Choice, MultiChoice), and for selecting
a plugin from available ones given constraint rules (Plugin).

From these declarations, parameter values are selected, taking into account
default values, preferences storage, and user interaction, and are
then applied to objects.  They are class methods, so we can query
parameters without the need to instanciate a Parametrizable object
first.

For more advanced use cases not (yet) supported by the decorator
syntax, declarations for the parameters taken by a Overlord.Parametrizable
instance can be expressed by overloading the `parameters_dcl()` class
method and the `set_params()` method.  `parameters_dcl()` must return
a dict of Overlord.ParamDecl keyed by string parameter identifiers,
aka. "paramid".  `set_params()` on the other end will be called with
`Overlord.Param` instance matching one of declarations, and is
expected to apply the given change to the object.

A helper mechanism is also provided to transform the internal
representation of a parameter (which is defined by the `ParamDecl`)
into an arbitrary object, transparently.  This can be done with the
with the `xformer` mechanism, and is used much like a property's
`setter` (https://docs.python.org/3/library/functions.html#property)

Such manual implementation is necessary eg. in generic classes whose
parameters depend on the context: a generic GameRenderer for example
will present choices for rendering the game's holders (boards, hands,
bags, captures, etc), and will use this advanced support to
dynamically create as many Plugin parameters as required by the game
class (eg, one per holder type), which is taken from the context.

The parameters_dcl() method only has to add new parameters to the one
specified by parent classes.  Symetrically, the set_params() method
passes to the parent classes' set_params() only the parameters that it
did not use itself.

Custom set_params implementations (when they store the received Param
object), and other creators of Param objects (when they are not meant
to be attached to a Parametrizable objet) should take care to set its
`owner` property, to ensure that no more than one object can claim
ownership by mistake.  This is done automatically when using the
decorator, when set_param() attaches a Param to a Parametrizable.

==== Interacting with parameters

For the common case of `@Overlord.parameter`, a `ParameterAttribute`
object is created to mediate access to the parameter, using the Python
Descriptor Protocol (https://docs.python.org/3/howto/descriptor.html).
It ensures that any attempt to modify the parameter by assigning it a
value goes transparently through `Parametrizable.set_param()`, and
return the `Param`'s `value` when reading it (possibly subject of an
additional `xformer` filter).

The `ParameterAttribute` object is a class attribute of the
Parametrizable object, and can be accessed as `type(obj).myparam`.
The `Param` object is hidden inside the object itself, can be accessed
through the `ParameterAttribute.fget_param()` method, as
e.g. `type(obj).myparam.fget_param(obj)`.

==== Storing preferences

The Preferences system is naturally built on the Parametrizable stuff.
Omaha has a single Preferences repository (today only kept in memory,
but which is intended to be saved on disk for persistence once we are
sure it is adequate), which stores those parameters and the context
they are related to, from one game to another.


=== Plugin Choice Sub-parameters

Plugin parameters choices can be parametrizable classes.  Those
classes' parameters are represented, for the purpose of preferences
storage and of GUI presentation, as sub-parameters of this choice.  A
`Param` for a `Choice` declaration keeps track, for each of the
possible choices, of their parameters, as `Param.subparams`.  Those
sub-parameters are create with the Choice's context augmented by the
choice itself, so choices can have the same kind of parameter (eg. a
graphical theme for rendering) but each choice can have a different
preference for that parameter.

Today only `params.Choice` implements sub-parameters, but
`params.MultiChoice` should too)

Subparams do not exist in a Plugin param from the start.  For the
purpose of user interaction, a Choice declaration is created for each
Plugin declaration, with subparams for each of the possible choices.
Subparams are then injected back into the Plugin Param.

NOTE: applying subparams is left to set_params, which should call
`Parametrizable._apply_pluginparams()` for this.  This should be part
of params.Plugin instead, and will change.

=== Plugability

A Overlord plugin-enabled application is a python application
containing at least one class derived from Overlord.Parametrizable,
with at least one parameter of class Overlord.params.Plugin.

Any python class can be loaded as a plugin (any should be, although today
it is limited to Parametrizable subclasses).  As an example, in the
Omaha board-game GUI, the generic UI class allows to launch games
(chess, checkers, etc) based on the available Game plugins, and allows
to use various ways to enter moves and to play against computer
players, based on available PlayerDriver plugins.

Plugins can also themselves be derived from Overlord.Parametrizable,
and define new types of plugins for their own use.
Eg. in Omaha, the StackedVector rendering engine for boardgame pieces
can be skinable through the use of "PieceSkin" plugins.

A plugin is declared to Overlord using a Desktop Entry (.desktop) file
as defined by FreeDesktop.org.  For this, an application defines its
own set of DesktopEntry types (eg. X-Omaha-Game, X-Omaha-GameRenderer,
X-Omaha-HolderRenderer), as well as any custom fields it requires for
its purpose.

Plugin declaration files are looked for in
/usr/share/games/omaha/plugins/ (which is structured by plugin type
for convenience, although the location within that tree is ignored).
For developing, and for people wanting to run from the source tree, a
"plugins" directory in the current working directory is used instead
if it is found.

The following FreeDesktop-standard fields are used:

 Name, Name[xx]      : name displayed to the user
 Type                : type of plugin (X-Omaha-*)
 TryExec             : (optional) only enables the plugin if this program is found
 Icon                : (optional) may be displayed by the UI
 Comment, Comment[xx]: (optional) may be displayed by the UI

The following fields are defined for all Overlord plugins, unless
otherwise stated:

 X-Omaha-ID          : identifier for the plugin, should be unique

 X-Omaha-*-Categories: with which other plugins of given type this plugin is
		       compatible, see description of specific plugin types
		       for details.

 X-Omaha-TryImport-{0..}: lists of python packages that must be available for
                          a python-based plugin to be considered `Usable()`.
                          There may be several alternative sets of packages
                          supported by a single plugin (eg. PyQt or PySide);
                          all modules in a set must be available for the set
                          to be valid; a single valid set marks the plugin as
                          usable.

Most plugins are implemented by a python class.  Those also provide:

 X-Omaha-Package     : python package holding the plugin implementation
 X-Omaha-Class       : name of the class inside the package specified by
		       X-Omaha-Package


== Design notes

=== Parametrizable

The implementation of the @parameter decorator in
Overlord.Parametrizable relies on a ParameterAttribute class, which
implements the python descriptor protocol to handle access to an
Overlord.Param object attached as private attribute or the
Parametrizable object.  The setter ensures a Param is stored, the
getter extracts the Param's value, and applies any transformation
using xformer().



== Design issues

=== Parametrizable initialization

Currently the initialization of parameters is done in
ParametrizableType.__call__() after the upcall to type.__call__(),
which means they are not seen from within __init__().


== Short term TODO list

See Omaha for possibly more.

=== Known bugs

- Linking class to desc through 'x-plugin-desc' is broken when using
  parameter specialization (several plugin descs for a single class).
- depends_on handling is broken when one param depends on several others,
  but luckily we do not need that yet (see pylint's cell-var-from-loop
  warning)

=== Required evolutions

- custom FreeDesktop fields use the `X-Omaha` prefix, should be `X-Overlord`
- pinning (or otherwise refining) a parameter decl in a subclass
  requires a way to clone a ParamDecl (done)
- parameter pinning works only by chance (and not for a default plugin choice)
  * pinned_value's should be set from X-Omaha-Parameter-* in Overlord.Plugin.Class()...
  * ...but a Parametrizable does not cache its parameter's decls
  * special-casing of pinned params should only occur in walk_action, not in
    walk_params or PluginChoiceWidget or GtkChoiceWidget, but in fact today only
    PluginChoiceWidget seems to matter
