#!/usr/bin/env python
from setuptools import setup, find_packages

## real setup work

setup(name='overlord',
      description='A (wannabe) component framework for the Omaha board games GUI.',
      url='https://gitlab.com/omaha-bgg/overlord',
      author='Yann Dirson',
      author_email='ydirson@free.fr',

      python_requires = ">3.9",
      setup_requires = ['setuptools_scm'],
      use_scm_version=True,

      packages = find_packages(),
      package_data={"Overlord": ["py.typed"]},

      install_requires=[
          'pyxdg',
          'typing_extensions >= 4.0; python_version<"3.11"',
      ],
)
