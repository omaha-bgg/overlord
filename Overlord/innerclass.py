# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = ['OuterClass']

import functools
from typing import Any, Sequence, Set

def get_type_attr(obj: object, attrname: str) -> type:
    attr = getattr(obj, attrname)
    if not isinstance(attr, type):
        raise TypeError(f"{attr} is not a type")
    return attr

class OuterClass(type):
    def __init__(cls, name: str, bases: tuple[type,...], attribs: dict[str,Any]):
        inherited_inners: set[str] = set().union(*[base.__innerclasses__ for base in bases
                                                   if isinstance(base, OuterClass)])
        # check we don't violate type restriction
        for inherited_inner in inherited_inners:
            # the candidate that would be selected by python
            innerattr: type = get_type_attr(cls, inherited_inner)

            if inherited_inner in attribs:
                for base in bases:
                    if hasattr(base, inherited_inner):
                        errordict={
                            "subcls": name, "supercls": base, "inner": inherited_inner,
                            "subinner": innerattr,
                            "superinner": get_type_attr(base, inherited_inner)}
                        assert isinstance(innerattr, type), \
                            "{subcls}.{inner} ({subinner}) is not a class".format(**errordict)
                        assert issubclass(innerattr, get_type_attr(base, inherited_inner)), \
                            ("{subcls}.{inner} ({subinner}, mro={mro}) is not a subclass of "
                             "{supercls}.{inner} ({superinner})").format(mro=innerattr.mro(),
                                                                         **errordict)
            else:
                candidates: list[type] = [get_type_attr(base, inherited_inner) for base in bases
                                          if hasattr(base, inherited_inner)]
                # most specific subclasses in candidates
                shortlist: Sequence[type] = functools.reduce(cls.__extend_shortlist, candidates, [])

                assert len(shortlist) == 1, (
                    f"{name}.{inherited_inner} cannot be chosen in {candidates}, "
                    "no single one is most specific, shortlist={shortlist}")

                # correct the candidate that would be selected by python if needed
                if innerattr is not shortlist[0]:
                    setattr(cls, inherited_inner, shortlist[0])

        super().__init__(name, bases, attribs)

        cls.__innerclasses__: set[str] # FIXME insufficient to catch type errors in client code
        try:
            new_inners: Set[str] = attribs['__innerclasses__']
        except KeyError:
            cls.__innerclasses__ = inherited_inners
        else:
            assert new_inners is cls.__innerclasses__
            cls.__innerclasses__ = inherited_inners.union(new_inners)

    @staticmethod
    def __extend_shortlist(classes: Sequence[type], newclass: type) -> Sequence[type]:
        # only keep those newclass does not derive from
        surviving = [c for c in classes
                     if not issubclass(newclass, c)]
        # only add newclass if we don't have a derivative yet
        for c in surviving:
            if issubclass(c, newclass):
                break
        else:
            surviving.append(newclass)
        return surviving
