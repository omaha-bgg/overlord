# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# pylint plugin to teach Pylint/Astroid about (some) Overlord
# specificities

from __future__ import annotations
from typing import TYPE_CHECKING

import astroid                  # type: ignore
if TYPE_CHECKING:
    import pylint.lint

def transform_function(node: astroid.nodes.NodeNG) -> astroid.nodes.NodeNG:
    # consider @Overlord.parameter as @classmethod
    if node.decorators:
        for d in node.decorators.nodes:
            #print iter(d.infer()).next().qname()
            #print [x.qname() for x in iter(d.infer())]
            if isinstance(d, astroid.Attribute):
                if d.attrname == 'parameter' and d.expr.name == 'Overlord':
                    node.type = 'classmethod'
    return node

def register(linter: pylint.lint.PyLinter) -> None: # pylint: disable=unused-argument
    astroid.MANAGER.register_transform(astroid.nodes.FunctionDef,
                                       transform_function)
