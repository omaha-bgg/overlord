# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Misc utility functions.
"""

import abc
import collections.abc
import os
import sys
from importlib.util import find_spec
from typing import Any, Optional, Sequence, TypeVar, Union
if sys.version_info >= (3, 10):
    from typing import TypeGuard
else:
    from typing_extensions import TypeGuard

def class_fullname(cls: type) -> str:
    """Return the full name of the given class, including package name."""
    assert isinstance(cls, type), f"{cls!r} is not a type"
    return f"{cls.__module__}.{cls.__name__}"

def __is_exe_file(path: str) -> bool:
    return os.path.exists(path) and os.access(path, os.X_OK)
def which(command: str) -> str:
    "Returns full path of command if found in PATH, or if valid absolute command."
    if os.path.isabs(command):
        if __is_exe_file(command):
            return command
        raise IOError(f"No such command {command}")
    for path in os.environ['PATH'].split(os.pathsep):
        fullname = os.path.join(path, command)
        if __is_exe_file(fullname):
            return fullname
    raise IOError(f"No such command {command} in PATH")

class PrettyAble(abc.ABC):
    @abc.abstractmethod
    def __pretty__(self, indent: int, curindent: int) -> str:
        pass

def pretty(obj: Union[PrettyAble,Any], indent: int=2, curindent:int=0) -> str:
    def __prettylst(lst: list[str], start: str, end: str, indent: int, curindent: int) -> str:
        if len(lst) == 0:
            return start + " " + end
        if len(lst) == 1:
            return start + " " + lst[0] + " " + end
        return (start + "\n" +
                ",\n".join([ ' ' * (curindent+indent) + e for e in lst ]) +
                '\n' + ' ' * curindent + end)

    if hasattr(obj, "__pretty__"):
        return obj.__pretty__(indent, curindent)
    if isinstance(obj, dict):
        return __prettylst([ "%s: %s" % (pretty(k, indent=indent,
                                                curindent=curindent+indent),
                                         pretty(v, indent=indent,
                                                curindent=curindent+indent))
                             for k, v in obj.items() ],
                           "{", "}", indent, curindent)
    if isinstance(obj, tuple):
        return __prettylst([ pretty(e, indent=indent,
                                    curindent=curindent+indent)
                             for e in obj ],
                           "(", (",)" if len(obj)==1 else ")"),
                           indent, curindent)
    if isinstance(obj, list):
        return __prettylst([ pretty(e, indent=indent,
                                    curindent=curindent+indent)
                             for e in obj ],
                           "[", "]", indent, curindent)
    if isinstance(obj, set):
        return __prettylst([ pretty(e, indent=indent,
                                    curindent=curindent+indent)
                             for e in obj ],
                           "{", "}", indent, curindent)
    # fallback to repr()
    return repr(obj)

def hashable(obj: Union[collections.abc.Hashable,
                        collections.abc.Mapping[collections.abc.Hashable,collections.abc.Hashable],
                        set[collections.abc.Hashable],
                        ]) -> collections.abc.Hashable:
    if isinstance(obj, collections.abc.Hashable):
        return obj
    if isinstance(obj, collections.abc.Mapping):
        return frozenset([ (k, hashable(v)) for k, v in obj.items() ])
    if isinstance(obj, set):
        return frozenset(obj)
    raise NotImplementedError(f"does not know how to make {obj} hashable")

def method_mro(obj: object, method_name: str) -> list[str]:
    return [ (class_fullname(k) if method_name in k.__dict__
              else f"({class_fullname(k)})")
             for k in type(obj).mro() ]
def classmethod_mro(cls: type, method_name: str) -> list[str]:
    return [ (class_fullname(k) if method_name in k.__dict__
              else "({class_fullname(k)})")
             for k in cls.mro() ]

# from http://stackoverflow.com/questions/128573/using-property-on-classmethods
# solution by Jason R. Coombs
class classproperty(property):  # pylint: disable=invalid-name
    def __get__(self, cls: object, owner: Optional[type] = None) -> Any:
        assert self.fget
        return self.fget.__get__(None, owner)() # pylint: disable=no-member

def have_module(name: str) -> bool:
    "Is named module available to python?"
    try:
        spec = find_spec(name)
        return spec is not None
    except ModuleNotFoundError:
        return False

_T = TypeVar("_T")

def is_none_free_sequence(seq: Sequence[Union[None,_T]]) -> TypeGuard[Sequence[_T]]:
    """Return True if `seq` does not contain None.

    Especially useful to help the type checker."""
    return not None in seq
