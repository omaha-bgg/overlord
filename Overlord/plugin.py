# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Plugin', 'InvalidPlugin' ]

import importlib
from importlib import resources
import logging
import os
import sys
from typing import Optional

from xdg.DesktopEntry import DesktopEntry, ParsingError # type:ignore

from .exceptions import NoSuchPlugin
from .misc import which, have_module

if sys.version_info >= (3, 12):
    from importlib.resources.abc import Traversable
else:
    from importlib.abc import Traversable

logger = logging.getLogger(__name__)

class InvalidPlugin(Exception):
    args: tuple[str]

class Plugin():
    "An xdg.DesktopEntry-based plugin."

    def __init__(self, traversable: Traversable, plugintype: str, pythonplugin: bool=True):
        if not traversable.name.endswith('.desktop'):
            raise NoSuchPlugin(f'"{traversable}" does not have .desktop suffix')

        self.__entry: DesktopEntry = DesktopEntry()
        try:
            with resources.as_file(traversable) as extracted_file:
                self.__entry.parse(extracted_file)
        except ParsingError as ex:
            raise InvalidPlugin(f'Error parsing "{traversable}", ignoring it') from ex

        if not self.__entry.hasKey('Type'):
            raise InvalidPlugin(f"{self.__entry!r} has no declared type")
        if self.__entry.get('Type') != plugintype:
            raise NoSuchPlugin(f"{self.__entry!r} has type {self.__entry.get('Type')!r}, "
                               f"requested type was {plugintype!r}")

        self.__usable: bool = True
        if self.__entry.hasKey('TryExec'):
            try:
                which(self.__entry.get('TryExec'))
            except IOError:
                # exe not found
                self.__usable = False

        if pythonplugin:
            # FIXME: no reason to have this check only for pythonplugin=True
            if not self.__entry.hasKey('X-Omaha-Package'):
                raise NoSuchPlugin(
                    f"{plugintype} {traversable} not matching pythonplugin={pythonplugin}")
            if not self.__entry.hasKey('X-Omaha-Class'):
                raise InvalidPlugin(f"{plugintype} plugin {traversable} without X-Omaha-Class")
            if self.__usable:
                has_tryimport = False
                found_imports = False
                for i in range(20): # FIXME should not have a limit
                    key = f'X-Omaha-TryImport-{i}'
                    if not self.__entry.hasKey(key):
                        break
                    has_tryimport = True
                    for testmod in self.__entry.get(key, list=True):
                        if not have_module(testmod):
                            logger.debug("%s: %s=%s FAILED", traversable, key, testmod)
                            break
                        logger.debug("%s: %s=%s found", traversable, key, testmod)
                    else:
                        found_imports = True
                        logger.debug("%s: %s OK", traversable, key)
                    if found_imports:
                        break
                if has_tryimport and not found_imports:
                    self.__usable = False
                    logger.debug("plugin %r fails TryImport", traversable)

        self.__path: str = str(traversable)
        self.__class: Optional[type] = None
        self.__pythonplugin: bool = pythonplugin

    @property
    def ID(self) -> str:
        return self.datafield('X-Omaha-ID')
    @property
    def name(self) -> str:
        return self.__entry.getName()
    @property
    def comment(self) -> str:
        return self.__entry.getComment()
    @property
    def pythonplugin(self) -> bool:
        return self.__pythonplugin
    @property
    def usable(self) -> bool:
        return self.__usable
    @property
    def directory(self) -> str:
        return os.path.dirname(self.__entry.filename)
    @property
    def icon(self) -> str:
        icon = self.__entry.getIcon()
        if icon != '':
            icon = f"{self.directory}/{icon}"
        return icon

    def __repr__(self) -> str:
        return f"<Overlord.Plugin({self.__path!r}) at 0x{id(self):x}>"

    def datafield(self, field: str) -> str:
        return self.__entry.get(field)
    def has_datafield(self, field: str) -> bool:
        return self.__entry.hasKey(field)
    def listfield(self, field: str) -> list[str]:
        return self.__entry.get(field, list=True)

    @property
    def Class(self) -> type:
        #if not self.__usable:
        #    # FIXME: we cannot do that yet, as .Class is used by group_py
        #    raise RuntimeError("Plugin %r is not usable" % (self.name,))
        if not self.__class:
            self.__class = _import_class_from(
                self.datafield('X-Omaha-Package'),
                self.datafield('X-Omaha-Class'))
            setattr(self.__class, 'x-plugin-desc', self)

        return self.__class

def _import_class_from(modulename: str, classname:str) -> type:
    """Import named module and return named class thereof."""
    return getattr(importlib.import_module(modulename), classname)
