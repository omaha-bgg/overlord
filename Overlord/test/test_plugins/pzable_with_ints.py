import Overlord
from Overlord.misc import classproperty

class PzableWithInts(Overlord.Parametrizable):
    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return PzableWithInts

    @Overlord.parameter
    def myint(cls, context: Overlord.Context) -> Overlord.params.Int:
        return Overlord.params.Int(label="My Int", default=1)
