# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys
import unittest
from typing import TYPE_CHECKING, Any, Optional, Union

import Overlord
from Overlord.misc import classproperty

# pylint: disable=invalid-name

thisdir = os.path.dirname(__file__)
(thistest, _) = os.path.splitext(os.path.basename(__file__))
sys.path.append(os.path.join(thisdir, "test_plugins"))
resource_module = "Overlord.test.test_plugins"

# this typedecl only because pylint does not grok the following import
PzableWithInts: Overlord.parametrizable.ParametrizableType
if TYPE_CHECKING:
    # mypy does not grok the sys.path munging above
    from .test_plugins.pzable_with_ints import PzableWithInts
else:
    # for some reason pylint does not look at this like inside conditional, otherwise:
    # pylint: disable=import-error,wrong-import-order,wrong-import-position
    from pzable_with_ints import PzableWithInts

# FIXME: all this gymnastics around myplugin emphasizes progress to be
# done with Plugin parameters
class PzableWithPlugin(Overlord.Parametrizable):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.myplugin = None

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return PzableWithPlugin

    @Overlord.parameter
    def myplugin_plugin(cls, context: Overlord.Context) -> Overlord.params.Plugin:
        return Overlord.params.Plugin(label="My plugin",
                                      plugintype = 'X-Omaha-Test',
                                      context=context)

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        parentparams = dict(params)
        myplugin_param: Optional[Overlord.Param[Overlord.Plugin]] = None
        for paramid, param in params.items():
            if paramid == "myplugin_plugin":
                assert isinstance(param, Overlord.Param)
                # HACK: keep to apply subparams
                myplugin_param = param
        super().set_params(parentparams)
        # propagate myplugin change
        if myplugin_param is not None:
            self._apply_pluginparams("myplugin", myplugin_param)
            #assert isinstance(self.myplugin, PzableWithInts)

class test00_plugin_subparams(unittest.TestCase):
    def setUp(self) -> None:
        plugin_manager = Overlord.PluginManager(resource_module)
        self.app = Overlord.App(plugin_manager=plugin_manager,
                                parentcontext=Overlord.Context(plugin_manager=plugin_manager))
        self.o = PzableWithPlugin(parentcontext=self.app.context)

    def test00_defaults(self) -> None:
        "subparams defaults to proper values"
        assert self.o.myplugin is not None
        self.assertEqual(type(self.o.myplugin_plugin), Overlord.Plugin)
        self.assertEqual(type(self.o.myplugin), PzableWithInts)
        self.assertEqual(self.o.myplugin.myint, 1)
