# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import re
import unittest

from Overlord import innerclass

# pylint: disable=unused-variable,invalid-name

class test00_basic(unittest.TestCase):
    "basic inner-class behaviour"
    def setUp(self) -> None:
        class A(metaclass=innerclass.OuterClass):
            __innerclasses__ = {'I'}
            class I():
                pass
        self.A = A
    def test00_good(self) -> None:
        "derived class can overload inner class with derived class"
        class B(self.A):
            class I(self.A.I):
                pass
    def test01_bad(self) -> None:
        "derived class cannot overload inner class with non-derived class"
        with self.assertRaises(AssertionError) as cm:
            class B(self.A):
                class I():
                    pass
        ex = cm.exception
        self.assertTrue(re.match(r"B\.I \(.*\) is not a subclass of ", ex.args[0]),
                        "Innerclass should be forbidden not to be a specialized version"
                        " of its parents")
    def test02_empty(self) -> None:
        "derived class can passthrough inner class"
        class C(self.A):
            pass

class test10_multi(unittest.TestCase):
    "basic behaviour on diamond inheritance"
    def setUp(self) -> None:
        class A(metaclass=innerclass.OuterClass):
            __innerclasses__ = {'I'}
            class I():
                pass
        class B1(A):
            class I(A.I):
                pass
        class B2(A):
            class I(A.I):
                pass
        class C2(B2):
            class I(B2.I, B1.I):
                pass
        self.A, self.B1, self.B2, self.C2 = A, B1, B2, C2
    def test00_good(self) -> None:
        "diamond-derived class can diamond-derive inner class"
        class C(self.B1, self.B2):
            class I(self.B1.I, self.B2.I):
                pass
    def test01_bad(self) -> None:
        "diamond-derived class cannot overload inner class with non-derived class"
        with self.assertRaises(AssertionError) as cm:
            class C(self.B1, self.B2):
                class I():
                    pass
        ex = cm.exception
        self.assertTrue(re.match(r"C\.I \(.*\) is not a subclass of ", ex.args[0]),
                        "Innerclass should be forbidden not to be a specialized version"
                        " of its parents")
    def test10_partiallybad(self) -> None:
        "diamond-derived class cannot overload inner class one derived from one not both"
        with self.assertRaises(AssertionError) as cm:
            class C(self.B1, self.B2):
                class I(self.B1):
                    pass
        ex = cm.exception
        self.assertTrue(re.match(r"C\.I \(.*\) is not a subclass of ", ex.args[0]),
                        "Innerclass should be forbidden not to be a specialized version"
                        " of its parents")
    def test20_implicitlybad(self) -> None:
        "diamond-derived class not defining inner class explicitly on ambiguity"
        with self.assertRaises(AssertionError) as cm:
            class C(self.B1, self.B2):
                pass
        ex = cm.exception
        self.assertTrue(ex.args[0].startswith("C.I cannot be chosen in "),
                        "Inherited innerclass should be forbidden not to be a specialized version"
                        " of all its parents")
    def test21_implicitlygood(self) -> None:
        "diamond-derived class can inherit diamond-derived inner class"
        class D(self.B1, self.C2):
            pass
    def test30_othergoodinherited(self) -> None:
        "mixing with unrelated class can passthrough inner class"
        class O():
            pass
        class C(self.A, O):
            pass
    def test31_othergoodoverload(self) -> None:
        "mixing with unrelated class can specialize inner class"
        class O():
            pass
        class C(self.A, O):
            class I(self.A.I):
                pass
    def test32_otherclash(self) -> None:
        "mixing with function member conflicting with inner class"
        class O():
            def I(self) -> None:
                pass
        with self.assertRaises(TypeError) as cm:
            class C(self.A, O):
                pass
        ex = cm.exception
        self.assertTrue(ex.args[0].endswith(" is not a type"),
                        "Name clash between inner-class and non-class should fail")

class test11_complexmulti(unittest.TestCase):
    def setUp(self) -> None:
        class A(metaclass=innerclass.OuterClass):
            __innerclasses__ = {'I'}
            class I():
                pass
        class B1(A):
            class I(A.I):
                pass
        class B2(A):
            class I(A.I):
                pass
        class C1(B1):
            pass
        class C2(B2):
            pass
        class D(B1, B2):
            class I(B1.I, B2.I):
                pass
        self.A, self.B1, self.B2, self.C1, self.C2, self.D = A, B1, B2, C1, C2, D
    def test00_implicitlygood(self) -> None:
        "3rd-level inheritance from diamond- and simply-derived gets implicitly-derived inner"
        class E(self.C1, self.C2, self.D):
            pass
    def test01_implicitlygood(self) -> None:
        "3rd-level inheritance from diamond- and more generic simply-derived coming later in mro"
        class E(self.D, self.C2):
            pass

class test20_chained(unittest.TestCase):
    "chaining of inner-class definitions"
    def setUp(self) -> None:
        class A(metaclass=innerclass.OuterClass):
            __innerclasses__ = {'I'}
            class I():
                pass
        class B(A):
            __innerclasses__ = {'J'}
            class J():
                pass
        class C(B):
            pass
        self.A, self.B, self.C = A, B, C
    def test00_good(self) -> None:
        "grandchild can overload both parent's and grandparent's inners"
        class D(self.C):
            class I(self.A.I):
                pass
            class J(self.B.J):
                pass
    def test10_bad_lower(self) -> None:
        "grandchild cannot overload parent's inner with unrelated"
        with self.assertRaises(AssertionError) as cm:
            class D(self.C):
                class I(self.A.I):
                    pass
                class J():
                    pass
        ex = cm.exception
        self.assertTrue(re.match(r"D\.J \(.*\) is not a subclass of ", ex.args[0]),
                        "Innerclass should be forbidden not to be a specialized version"
                        " of its parents")
    def test11_bad_upper(self) -> None:
        "grandchild cannot overload grandparent's inner with unrelated"
        with self.assertRaises(AssertionError) as cm:
            class D(self.C):
                class I():
                    pass
                class J(self.B.J):
                    pass
        ex = cm.exception
        self.assertTrue(re.match(r"D\.I \(.*\) is not a subclass of ", ex.args[0]),
                        "Innerclass should be forbidden not to be a specialized version"
                        " of its parents")
