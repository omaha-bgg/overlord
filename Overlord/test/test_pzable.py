# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import unittest
from typing import Any, Union

import Overlord
from Overlord.misc import classproperty
from Overlord.preferences import prefs

# pylint: disable=invalid-name

resource_module = "Overlord.test.test_plugins.empty"

class rectangle(Overlord.Parametrizable):
    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return rectangle

    @Overlord.parameter
    def width(cls, context: Overlord.Context # pylint: disable=unused-argument
              ) -> Overlord.params.Int:
        return Overlord.params.Int(label="Width",
                                   minval=0, maxval=100,
                                   default=10)
    @Overlord.parameter
    def height(cls, context: Overlord.Context # pylint: disable=unused-argument
               ) -> Overlord.params.Int:
        return Overlord.params.Int(label="Height",
                                   minval=0, maxval=100,
                                   default=13)

class square(rectangle):
    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        super().set_params(params)
        if self.width != self.height:
            raise Overlord.ParameterError(
                f"square cannot have inequal dimensions, {self.width} != {self.height}")

class test_simple(unittest.TestCase):
    def setUp(self) -> None:
        # ensure prefs are empty
        setattr(Overlord.preferences, "__prefs", Overlord.preferences.Preferences())

        plugin_manager=Overlord.PluginManager(resource_module)
        self.app = Overlord.App(parentcontext=Overlord.Context(plugin_manager=plugin_manager),
                                plugin_manager=plugin_manager)
        self.r1 = rectangle(parentcontext=self.app.context)
        self.s1 = square(parentcontext=self.app.context,
                         paramvalues={"width": 20, "height": 20})

    def test00_defaults(self) -> None:
        "pzable's defaulted int parameter seen by getters"
        self.assertEqual(self.r1.width, 10)
        self.assertEqual(self.r1.height, 13)
    def test01_set_get(self) -> None:
        "pzable's int parameter setter operation seen by getter"
        self.r1.width = 15
        self.assertEqual(self.r1.width, 15)
    def test02_derived_defaults(self) -> None:
        "derived-pzable's defaulted int parameter seen by getters"
        self.assertEqual(self.s1.width, 20)
        self.assertEqual(self.s1.height, 20)

    def test03_create_no_paramvalues(self) -> None:
        "check creation of a different pzable in same context"
        s2 = square(parentcontext=self.app.context)
        self.assertEqual(s2.width, 20) # the value stored in prefs
    def test04_create_paramvalues_override(self) -> None:
        "check creation of a different pzable in same context"
        s2 = square(parentcontext=self.app.context,
                    paramvalues={"width": 12, "height": 12})
        self.assertEqual(s2.width, 12)

    def test05_explicit_override(self) -> None:
        self.assertEqual(
            prefs().pref_get(((Overlord.App, Overlord.App),
                              (rectangle, square)), "width").value, 20)
        s2 = square(parentcontext=self.app.context,
                    paramvalues=dict(width=30, height=30))
        self.assertEqual(s2.width, 30)

    @unittest.expectedFailure
    def test10_outofbounds_max(self) -> None:
        "setting pzable's int parameter above max"
        with self.assertRaises(Overlord.ParameterError):
            self.r1.width = 101

    @unittest.expectedFailure
    def test11_outofbounds_min(self) -> None:
        "setting pzable's int parameter below min"
        with self.assertRaises(Overlord.ParameterError):
            self.r1.width = -1

    def test12_badtype(self) -> None:
        "setting pzable's int parameter with non-int"
        with self.assertRaises(ValueError):
            self.r1.width = "junk" # type: ignore[assignment]

    def test20_setparams_bare(self) -> None:
        "bare pzable's int set_param operation seen by getter"
        self.r1.set_params({"width": 18})
        self.assertEqual(self.r1.width, 18)

    def test30_create_inconsistent(self) -> None:
        "catch violation attempt of constraint in derived pzable class at construction time"
        ctx = self.app.context.clone()
        ctx.add('test', 'test_create_inconsistent')
        with self.assertRaises(Overlord.ParameterError):
            square(parentcontext=ctx,
                   paramvalues={"width": 20, "height": 25})

    def test31_violate_constraint(self) -> None:
        "catch violation attempt of constraint in derived pzable class from setter"
        with self.assertRaises(Overlord.ParameterError):
            self.s1.width += 1

    def test40_contextkey_clash(self) -> None:
        "catch creation in a context of a second object of base class for a single context key"
        with self.assertRaises(AssertionError):
            rectangle(parentcontext=self.r1.context)

    def test41_contextkey_clash_derived(self) -> None:
        "catch creation in a context of an object of derived class for a single context key"
        with self.assertRaises(AssertionError):
            square(parentcontext=self.r1.context)

    def test50_clone(self) -> None:
        "cloned pzable object gets same initial values for parameters"
        self.r1.width = 15
        r2 = self.r1.clone()
        self.assertEqual(r2.width, self.r1.width)
        self.assertEqual(r2.height, self.r1.height)

    def test51_clone_decoupling(self) -> None:
        "cloned pzable object parameters are independent from original"
        r2 = self.r1.clone()
        # pylint: disable=no-member
        self.assertNotEqual(rectangle.width.fget_param(self.r1),
                            rectangle.width.fget_param(r2))
        self.assertEqual(r2.height, self.r1.height)
        self.r1.height = 51
        self.assertNotEqual(r2.height, self.r1.height)

class test_derived_default(unittest.TestCase):
    def setUp(self) -> None:
        # ensure prefs are empty
        setattr(Overlord.preferences, "__prefs", Overlord.preferences.Preferences())

        plugin_manager=Overlord.PluginManager(resource_module)
        self.app = Overlord.App(parentcontext=Overlord.Context(plugin_manager=plugin_manager),
                                plugin_manager=plugin_manager)
        self.r1 = rectangle(parentcontext=self.app.context)
    @unittest.expectedFailure # currently no way to specify the default behaviour
    def test00_default_with_constraint(self) -> None:
        s1 = square(parentcontext=self.app.context)
        self.assertEqual(s1.width, s1.height)
