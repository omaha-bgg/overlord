# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from importlib import resources
import os
import sys
from Overlord import plugin

# pylint: disable=unused-variable,invalid-name

thisdir = os.path.dirname(__file__)
(thistest, _) = os.path.splitext(os.path.basename(__file__))
sys.path.append(os.path.join(thisdir, "test_plugins"))
resource_dir = resources.files("Overlord.test.test_plugins")

def test00_noclass() -> None:
    "loading a minimal plugin"
    p = plugin.Plugin(resource_dir.joinpath("noclass.desktop"),
                      "X-Omaha-Test", pythonplugin=False)

def test10_class() -> None:
    "loading a plugin that brings a class"
    p = plugin.Plugin(resource_dir.joinpath("thing.desktop"),
                      "X-Omaha-Test2")
    cls = p.Class
