# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import unittest
from collections import OrderedDict
from typing import Optional

from Overlord import params

# pylint: disable=invalid-name

# FIXME: missing from test coverage:
# - ParamDecl
#   - depends_on
#   - pinned_value setter
# - Param
#   - getting pinned value
#   - pretty-printing
#   - sub-params
#   - walk_params

class test00_string(unittest.TestCase):
    "test Overlord.params.String"
    def setUp(self) -> None:
        self.decl = params.String(label="my string",
                                  default="nothing")
        self.p = params.Param(self.decl)
    def test00_default(self) -> None:
        "defaulted String param value seen by getter"
        self.assertEqual(self.p.value, "nothing")
    def test10_set(self) -> None:
        "String param value setter operation seen by getter"
        self.p.value = "something"
        self.assertEqual(self.p.value, "something")

    # FIXME: should not stay
    def test20_string_cast(self) -> None:
        "implicit casting of int to String"
        self.p.value = 3        # type: ignore[assignment]
        self.assertEqual(self.p.value, "3")

    def test30_clone(self) -> None:
        clone = self.decl.clone()


class test10_int_bounds(unittest.TestCase):
    "test Overlord.params.Int"
    def setUp(self) -> None:
        self.decl: params.Int = params.Int(label="i", default=0, minval=-10, maxval=100)
        self.p = params.Param(self.decl)
        assert self.p.decl is self.decl
    def test00_limit_low(self) -> None:
        "setting Int value to low bound"
        self.p.value = -10
        self.assertEqual(self.p.value, -10)
    def test01_limit_high(self) -> None:
        "setting Int value to high bound"
        self.p.value = 100
        self.assertEqual(self.p.value, 100)
    @unittest.expectedFailure
    def test10_offlimit_low(self) -> None:
        "setting Int value below low bound"
        self.p.value = -11
        self.assertNotEqual(self.p.value, -11)
    @unittest.expectedFailure
    def test11_offlimit_high(self) -> None:
        "setting Int value above high bound"
        self.p.value = 101
        self.assertNotEqual(self.p.value, 101)
    def test20_limitchange(self) -> None:
        "changing Int bound"
        assert isinstance(self.p.decl, params.Int)
        self.p.decl.max = 50
        self.assertEqual(self.p.decl.max, 50)
        #self.p.decl.min = 0
        #self.assertEqual(self.p.decl.min, 0)
    @unittest.expectedFailure
    def test21_limitchange_value(self) -> None:
        "changing Int bound making previous value out-of-bounds"
        assert isinstance(self.p.decl, params.Int)
        self.assertEqual(self.p.decl.max, 100)
        self.p.value = 75
        self.p.decl.max = 50
        self.assertEqual(self.p.decl.max, 50)
        self.assertEqual(self.p.value, self.p.decl.max)
    @unittest.expectedFailure
    def test22_offlimitchange_max(self) -> None:
        "attempt changing Int high bound to be below low bound"
        assert isinstance(self.p.decl, params.Int)
        self.p.decl.max = -100
        self.assertNotEqual(self.p.decl.max, -100)

    def test30_clone(self) -> None:
        clone = self.decl.clone()

class test20_choice(unittest.TestCase):
    "test Overlord.params.Choice"
    def setUp(self) -> None:
        self.decl = params.Choice(label="choice",
                                  alternatives={"first": 1, "second": 2, "tenth": 10},
                                  default=2)
        self.p = params.Param(self.decl)
    def test00_default(self) -> None:
        "Choice param default value"
        self.assertEqual(self.p.value, 2)
    def test01_set(self) -> None:
        "setting Choice param to valid values"
        self.p.value = 1
        self.assertEqual(self.p.value, 1)
        self.p.value = 2
        self.assertEqual(self.p.value, 2)
        self.p.value = 10
        self.assertEqual(self.p.value, 10)
    def test10_setbad(self) -> None:
        "setting Choice to invalid value"
        with self.assertRaises(LookupError):
            self.p.value = 3

    def test30_clone(self) -> None:
        clone = self.decl.clone()

class test21_choice_bad_default(unittest.TestCase):
    "test Overlord.params.Choice with a bad default"
    def test(self) -> None:
        self.decl = params.Choice(label="choice",
                                  alternatives=OrderedDict(first=1, second=2, tenth=10),
                                  default=3)
        self.p = params.Param(self.decl)
        with self.assertLogs("Overlord.params", level="WARNING"):
            # default should be first choice
            self.assertEqual(self.p.value, 1)

class test25_multichoice(unittest.TestCase):
    "test Overlord.params.MultiChoice"
    def test00_nodefault(self) -> None:
        d = params.MultiChoice(label="multichoice",
                               alternatives={"first": 1, "second": 2, "tenth": 10},
        )
        p = params.Param(d)
        self.assertEqual(p.value, [])
    def test01_onedefault(self) -> None:
        d = params.MultiChoice(label="multichoice",
                               alternatives={"first": 1, "second": 2, "tenth": 10},
                               default=[1],
        )
        p = params.Param(d)
        self.assertEqual(p.value, [1])
    def test02_manydefaults(self) -> None:
        d = params.MultiChoice(label="multichoice",
                               alternatives={"first": 1, "second": 2, "tenth": 10},
                               default=[1, 10],
        )
        p = params.Param(d)
        self.assertEqual(p.value, [1, 10])
    def test03_baddefault(self) -> None:
        d = params.MultiChoice(label="multichoice",
                               alternatives={"first": 1, "second": 2, "tenth": 10},
                               default=[1, 42],
        )
        p = params.Param(d)
        with self.assertLogs("Overlord.params", level="WARNING"):
            v = p.value
        self.assertEqual(v, [1])

    def test10_clone(self) -> None:
        d = params.MultiChoice(label="multichoice",
                               alternatives={"first": 1, "second": 2, "tenth": 10},
                               default=[1, 10],
        )
        d2 = d.clone()

    # FIXME test set_param

class test30_change_listener(unittest.TestCase):
    def setUp(self) -> None:
        self.decl: params.Int = params.Int(label="i", default=0, minval=-10, maxval=100)
        self.p = params.Param(self.decl)
        assert self.p.decl is self.decl

    def test01_listener_noarg(self) -> None:
        listener_called = False
        def param_change_listener(param: params.Param[params.ParamValue],
                                  oldvalue: Optional[params.ParamValue]) -> None:
            nonlocal listener_called
            listener_called = True
        self.p.register_changed(param_change_listener)
        self.assertFalse(listener_called)
        self.p.value = 2
        self.assertTrue(listener_called)

    def test02_listener_noarg_extra_arg(self) -> None:
        listener_called = False
        def param_change_listener(param: params.Param[params.ParamValue],
                                  oldvalue: Optional[params.ParamValue]) -> None:
            nonlocal listener_called
            listener_called = True

        # args never allowed, detected at register time (and by type checker)
        with self.assertRaises(TypeError):
            self.p.register_changed(param_change_listener, 42) # type: ignore[call-arg, arg-type]

        # extra kwarg detected at value-change time (and by type checker)
        self.p.register_changed(param_change_listener, foo=42) # type: ignore[call-arg, arg-type]
        with self.assertRaises(TypeError):
            self.p.value = 2
