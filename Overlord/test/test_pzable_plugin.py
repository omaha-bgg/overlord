# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys
import unittest

import Overlord
from Overlord.misc import classproperty

# pylint: disable=invalid-name

thisdir = os.path.dirname(__file__)
(thistest, _) = os.path.splitext(os.path.basename(__file__))
sys.path.append(os.path.join(thisdir, "test_plugins"))
resource_module = "Overlord.test.test_plugins"

class Top(Overlord.Parametrizable):
    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return Top
    @Overlord.parameter
    def first_thing(cls, context: Overlord.Context) -> Overlord.params.Plugin:
        return Overlord.params.Plugin(label="a thing",
                                      plugintype = 'X-Omaha-Test',
                                      context=context)

class test00_pzable_plugin(unittest.TestCase):
    "test Overlord.params.Plugin"
    def setUp(self) -> None:
        plugin_manager = Overlord.PluginManager(resource_module)
        self.app = Overlord.App(plugin_manager=plugin_manager,
                                parentcontext=Overlord.Context(plugin_manager=plugin_manager))
    def test00_default(self) -> None:
        "check a Plugin parameter defaults to a Plugin"
        a = Top(parentcontext=self.app.context)
        self.assertEqual(type(a.first_thing), Overlord.Plugin)
    def test01_class(self) -> None:
        "check a Plugin parameter provides a class"
        a = Top(parentcontext=self.app.context)
        cls = a.first_thing.Class
        assert isinstance(cls, type)
