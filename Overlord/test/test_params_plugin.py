# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys
import unittest

import Overlord
from Overlord import params

# pylint: disable=invalid-name

thisdir = os.path.dirname(__file__)
(thistest, _) = os.path.splitext(os.path.basename(__file__))
sys.path.append(os.path.join(thisdir, "test_plugins"))
resource_module = "Overlord.test.test_plugins"

class test00_plugin(unittest.TestCase):
    "test Overlord.params.Plugin"
    def setUp(self) -> None:
        self.plugin_manager = Overlord.PluginManager(resource_module)
        self.decl = params.Plugin(label="a thing",
                                  plugintype = 'X-Omaha-Test',
                                  context=Overlord.Context(plugin_manager=self.plugin_manager))
        self.p = params.Param(self.decl)
    def test00_default(self) -> None:
        "plugin param value is a Plugin object"
        self.assertEqual(type(self.p.value), Overlord.Plugin)
