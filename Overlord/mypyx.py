from typing import Callable, Optional
from mypy.plugin import Plugin, ClassDefContext # pylint: disable=no-name-in-module # !?
from mypy.nodes import Decorator, MemberExpr, TypeInfo # pylint: disable=no-name-in-module # !?

PZABLE_FULLNAME = "Overlord.parametrizable.Parametrizable"
DECORATOR_FULLNAME = "Overlord.parametrizable.parameter"

class OverlordPlugin(Plugin):
    def get_base_class_hook(self, fullname: str) -> 'Optional[Callable[[ClassDefContext], None]]':
        sym = self.lookup_fully_qualified(fullname)
        if sym and isinstance(sym.node, TypeInfo):  # pragma: no branch
            # No branching may occur if the mypy cache has not been cleared
            if any(base.fullname == PZABLE_FULLNAME for base in sym.node.mro):
                return _overlord_baseclass_hook
        return None

def _overlord_baseclass_hook(ctx: ClassDefContext) -> None:
    for _, sym in ctx.cls.info.names.items():
        if isinstance(sym.node, Decorator):
            first_dec = sym.node.original_decorators[0]
            if not isinstance(first_dec, MemberExpr):
                continue
            if first_dec.fullname != DECORATOR_FULLNAME:
                continue
            sym.node.func.is_class = True


def plugin(version: str) -> type[Plugin]: # pylint: disable=unused-argument
    return OverlordPlugin
