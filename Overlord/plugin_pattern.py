# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'PluginPattern' ]

import logging
from typing import Optional, Union

from .misc import class_fullname, is_none_free_sequence
from .plugin import Plugin

logger = logging.getLogger(__name__)

class PluginPattern():
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        """Returns a "distance" measure of `plugin` from the pattern.

        Returns None if `plugin` does not match.
        """
        raise NotImplementedError()

class AnyPluginPattern(PluginPattern):
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        return 0
    def __repr__(self) -> str:
        return "AnyPluginPattern()"
Any = AnyPluginPattern()

class Or(PluginPattern):
    def __init__(self, *patterns: PluginPattern):
        super().__init__()
        self.__patterns = patterns
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        for pat in self.__patterns:
            distance = pat.match_distance(plugin)
            if distance is not None:
                return distance
        return None
    def __repr__(self) -> str:
        return "Or(%s)" % ', '.join(repr(p) for p in self.__patterns)

class And(PluginPattern):
    def __init__(self, *patterns: PluginPattern):
        super().__init__()
        self.__patterns = patterns
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        distances: list[Optional[int]] = [ pat.match_distance(plugin) for pat in self.__patterns ]
        if not is_none_free_sequence(distances):
            return None
        return max(distances)
    def __repr__(self) -> str:
        return "And(%s)" % ', '.join(repr(p) for p in self.__patterns)

class ParentOf(PluginPattern):
    """Assumes cls.mro() does not change over the live of the pattern object."""
    def __init__(self, pluginkey: str, cls: type):
        assert isinstance(cls, type)
        super().__init__()
        self.__pluginkey, self.__cls = pluginkey, cls
        self.__parentnames: list[str] = [ class_fullname(parent_cls)
                                          for parent_cls in cls.mro() ]
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        candidates = plugin.listfield(self.__pluginkey)
        logger.debug("match_distance(%s) %s in %s ?", plugin, self.__parentnames, candidates)
        for candidate in candidates:
            try:
                distance = self.__parentnames.index(candidate)
            except ValueError:
                continue
            logger.debug("match_distance = %s", distance)
            return distance
        logger.debug("match_distance: no match")
        return None
    def __repr__(self) -> str:
        return f"ParentOf({self.__pluginkey!r}, {self.__cls!r})"

class NumericGreaterEqual(PluginPattern):
    def __init__(self, pluginkey: str, value: Union[int,float]):
        super().__init__()
        self.__pluginkey, self.__value = pluginkey, value
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        numstr = plugin.datafield(self.__pluginkey)
        if numstr == '':
            return None
        num = int(numstr)

        return 0 if num >= self.__value else None
    def __repr__(self) -> str:
        return f"NumericGreaterEqual({self.__pluginkey!r}, {self.__value})"

class Usable(PluginPattern):
    def match_distance(self, plugin: Plugin) -> Optional[int]:
        if plugin.usable:
            return 0
        return None
    def __repr__(self) -> str:
        return "Usable()"
