# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

__all__ = [ 'Preferences', 'prefs' ]

import sys
from typing import TYPE_CHECKING, Any

from .params import ContextKey, ContextValue, Param
from .misc import pretty

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias
if TYPE_CHECKING:
    from .parametrizable import Parametrizable

PrefsKey: TypeAlias = tuple[tuple[ContextKey, ContextValue], ...]

class Preferences():

    def __init__(self) -> None:
        self.__prefs: dict[PrefsKey,dict[str,Param[Any]]] = {}

    def pref_loaded(self, context: PrefsKey, paramid: str) -> bool:
        return (context in self.__prefs and
                paramid in self.__prefs[context])

    def pref_get(self, context: PrefsKey, paramid: str) -> Param[Any]:
        return self.__prefs[context][paramid]

    def record_param(self, context: PrefsKey, paramid: str, param: Param[Any]) -> None:
        """
        Record given param in specified context.  Any pre-existing
        entry is overwritten.
        """
        assert isinstance(param, Param)
        param.owner = self
        if context not in self.__prefs:
            self.__prefs[context] = {}
        self.__prefs[context][paramid] = param

    def get(self, pzable: Parametrizable) -> dict[str,Param[Any]]:
        """Get preferences of given Parametrizable object."""
        return dict(self.__prefs[pzable.context.hashable()])

    def dump(self, curindent: int) -> str:
        return pretty(self.__prefs, curindent=curindent)

# global Preferences object
__prefs = Preferences()

def prefs() -> Preferences:
    return __prefs
