# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

__all__ = [ 'Context', 'ContextKey', 'Param', 'ParamDecl', 'ParamValue' ]

import collections
from collections import OrderedDict
import logging
import sys
from typing import (TYPE_CHECKING, Any, Callable, Generic, Iterable,
                    Sequence, Optional, TypeVar, Union)
if sys.version_info >= (3, 10):
    from typing import Concatenate, ParamSpec
else:
    from typing_extensions import Concatenate, ParamSpec
if sys.version_info >= (3, 11):
    from typing import Self, TypeAlias
else:
    from typing_extensions import Self, TypeAlias

from .exceptions import NoSuchPlugin
from .misc import pretty, hashable
from . import plugin_pattern
from .plugin import Plugin as CorePlugin
from .plugin_manager import PluginManager
if TYPE_CHECKING:
    from .parametrizable import ParametrizableType

logger = logging.getLogger(__name__)
ContextKey: TypeAlias = "ParametrizableType"
ContextValue: TypeAlias = "ParametrizableType"

class Context():
    def __init__(self, plugin_manager: PluginManager):
        self.plugin_manager = plugin_manager
        self.__param_context: OrderedDict[ContextKey, ContextValue] = OrderedDict()
    def hashable(self) -> tuple[tuple[ContextKey, ContextValue], ...]:
        # keep item ordering
        return tuple([ (k, v) for k, v in self.__param_context.items() ])
    def add(self, key: ContextKey, value: ContextValue) -> None:
        #assert issubclass(key, Parametrizable) # FIXME cannot check, abstraction-level confusion
        assert key not in self.__param_context
        self.__param_context[key] = value
    def __contains__(self, key: ContextKey) -> bool:
        return key in self.__param_context
    def clone(self) -> Self:
        # pylint: disable=protected-access
        new = type(self)(self.plugin_manager)
        new.__param_context = OrderedDict(self.__param_context)
        return new
    def __getitem__(self, key: ContextKey) -> ContextValue:
        return self.__param_context[key]
    def __pretty__(self, indent: int, curindent: int) -> str:
        return (f"Context(mgr={self.plugin_manager}, "
                f"ctx={pretty(self.__param_context, indent, curindent+indent)})")

ParamValue = TypeVar("ParamValue")

class AbstractParamDecl(Generic[ParamValue]):
    "Base class for any Param declaration"
    def __has_deps_not_in__(self, ordered: Sequence[tuple[str,Param[ParamValue]]]) -> bool:
        raise NotImplementedError()

_P = ParamSpec("_P")

# FIXME this cannot express kwargs, so we use it as little as
# possible (only where we can't get a bound ParamSpec)
ParamChangedListener: TypeAlias = Callable[
    ["Param[ParamValue]", Optional[ParamValue]],
    None]
ParamDependsonListener: TypeAlias = Callable[
    ["Param[ParamValue]", ParamValue, "Param[Any]"],
    None]

class ParamDecl(AbstractParamDecl[ParamValue]):
    """Abstract class for parameter declarations."""
    def __init__(self, label: str, default: Optional[ParamValue]=None,
                 depends_on: Optional[dict[str,ParamDependsonListener[ParamValue]]]=None,
                 pinned_value: Optional[ParamValue]=None,
                 hidden: bool=False):
        self.__label = label
        self.__default = default
        self.__pinnedvalue = pinned_value
        self.__depends_on = depends_on or {}
        self.__hidden = hidden
    def clone(self, **override_kwargs: Any) -> Self:
        """Return a new ParamDecl with the same attributes, possibly overridden.

        Any argument will be passed to the constructor of the new
        ParamDecl.
        """
        kwargs = self.clonable_params()
        assert isinstance(kwargs, dict), f"{kwargs} is not a dict"
        kwargs.update(**override_kwargs)
        return type(self)(**kwargs)
    def clonable_params(self) -> dict[str,Any]:
        "A list of (paramname, value) pairs to be used by .clone()."
        return {"label": self.__label,
                "default": self.__default,
                "pinned_value": self.__pinnedvalue,
                "depends_on": self.__depends_on,
                "hidden": self.__hidden,
                }
    @property
    def label(self) -> str:
        return self.__label
    @property
    def depends_on(self) -> dict[str,ParamDependsonListener[ParamValue]]:
        return self.__depends_on
    def default(self) -> Optional[ParamValue]:
        """The default value for the parameter.

        When none was declared, the actual value may depend on the
        overall context, which we define to be the plugin_manager in use.
        """
        return self.__default
    @property
    def hidden(self) -> bool:
        "Whether a UI should hide this parameter from the user"
        return self.__hidden
    def cast(self, value: Any) -> ParamValue:
        """Cast `value` into something meaningful for this ParamDecl.

        When impossible, raise a `LookupError`.
        """
        if isinstance(value, Param[ParamValue] if TYPE_CHECKING else Param):
            assert type(value.decl) is type(self), f"{value} decl should be a {type(self)}"
            rawvalue: ParamValue = value.value
            return rawvalue
        raise LookupError()
    @property
    def pinned_value(self) -> Optional[ParamValue]:
        return self.__pinnedvalue
    @pinned_value.setter
    def pinned_value(self, v: ParamValue) -> None:
        self.__pinnedvalue = self.cast(v)
    def __pretty__(self, indent: int, curindent: int) -> str: # pylint: disable=unused-argument
        return (f"{type(self).__name__} ParamDecl labelled {self.label!r}"
                f"{', pinned' if self.__pinnedvalue else ''}"
                f"{', hidden' if self.__hidden else ''}")

    # AbstractParamDecl method implementations
    def __has_deps_not_in__(self, ordered: Sequence[tuple[str,Param[ParamValue]]]) -> bool:
        for depname in self.depends_on.keys():
            if depname not in [ paramid for paramid, _ in ordered ]:
                return True
        return False

class Param(Generic[ParamValue]):
    """
    An instance of a parameter declaration, including a value.
    """
    __logger = logging.getLogger('Overlord.params.Param')
    def __init__(self, decl: ParamDecl[ParamValue]):
        assert isinstance(decl, ParamDecl), f"{decl!r} is not a ParamDecl"
        self.__decl = decl
        self.__value: Optional[ParamValue] = None
        self.__owner: Optional[object] = None     # for assert checks only
        self.__changed_listeners: dict[ParamChangedListener[ParamValue],dict[str, Any]] = {}
        # FIXME: we should not know about that - paramdecl as
        # metaclass would help ?
        self.__subparams: dict[str,dict[str,Param[ParamValue]]]
        if hasattr(decl, 'subparams'):
            self.__subparams = dict(
                (parentid, fresh_parameters_for_decls(subparams))
                for parentid, subparams in decl.subparams.items() )
            logger.debug("%s: fresh subparams %s", self,
                         "{" + ', '.join(f"{k}: {pretty(v)}"
                                         for k, v in self.__subparams.items()) + "}")
        else:
            self.__subparams = {}
        self.__logger.debug("%s __init__'d", self)

    def clone(self) -> Self:
        """Return a new Param with the same value.
        """
        newparam = type(self)(self.decl)
        # FIXME?: instanciates None=="use default" to real default
        newparam.value = self.value
        # FIXME should make a deep copy of subparams
        self.__logger.debug("%s cloned to %s", self, newparam)
        return newparam

    @property
    def decl(self) -> ParamDecl[ParamValue]:
        return self.__decl
    @property
    def label(self) -> str:
        return self.__decl.label

    def register_changed(self,
                         listener: Callable[
                             Concatenate[Param[ParamValue], Optional[ParamValue], _P], None],
                         *args: _P.args, **kwargs: _P.kwargs) -> None:
        assert callable(listener)
        if args:
            raise TypeError("changed listener only accepts keyword args")
        assert listener not in self.__changed_listeners
        self.__changed_listeners[listener] = kwargs
    def unregister_changed(self, listener: Callable[
            Concatenate[Param[ParamValue], Optional[ParamValue], _P], None]) -> None:
        del self.__changed_listeners[listener]

    @property
    def value(self) -> ParamValue:
        if self.__decl.pinned_value is not None:
            return self.__decl.pinned_value
        if self.__value is not None:
            return self.__value
        default = self.__decl.default()
        if default is None:
            raise ValueError(f"parameter {self.label} has no value and no default")
        return default
    @value.setter
    def value(self, v: ParamValue) -> None:
        old = self.__value
        self.__logger.debug("%s set value=%s (was %s)", self, v, old)
        self.__value = self.__decl.cast(v)
        for listener, kwargs in self.__changed_listeners.items():
            listener(self, old, **kwargs)

    @property
    def owner(self) -> Optional[object]: # essentially "-> Parametrizable"
        "Object to which this Param belongs.  Can be set only once."
        return self.__owner
    @owner.setter
    def owner(self, v: object) -> None:
        assert self.__owner == None
        self.__logger.debug("%s owner is %s", self, v)
        self.__owner = v

    def __str__(self) -> str:
        return "<Overlord.Param at 0x{:x}, decl={}, value={}, sub={}>".format(
            id(self), self.__decl, self.__value,
            {k: str(v) for k, v in self.__subparams.items()})

    def __pretty__(self, indent: int, curindent: int) -> str:
        ret = "Param(0x%x %s): %s" % (id(self), pretty(self.__decl), self.value)
        if isinstance(self.value, CorePlugin):
            # pylint: disable=no-member
            if self.subparams and self.value.name in self.subparams:
                ret += " sub=" + pretty(self.subparams[self.value.name], indent, curindent+indent)
        return ret

    # FIXME see above
    @property
    def subparams(self) -> dict[str,dict[str,Param[ParamValue]]]:
        return self.__subparams
    @property
    def has_subparams(self) -> bool:
        return len(self.__subparams) != 0
    def set_subparams(self, subparams: dict[str,dict[str,Param[ParamValue]]]) -> None:
        # this is only a hack to init Plugin.subparams, don't use it for anything else
        assert isinstance(self.__decl, Plugin), \
            f"{self.__decl} is not a Plugin ({type(self.__decl)})"
        self.__subparams = subparams

# Helpers

def fresh_parameters_for_decls(decls: dict[str,ParamDecl[ParamValue]]
                               ) -> dict[str,Param[ParamValue]]:
    return dict( (paramid, Param(decl)) for paramid, decl
                 in decls.items() )

# Concrete parameter classes

class String(ParamDecl[str]):
    def __init__(self, label: str, default: str="", **kwargs: Any):
        assert isinstance(default, str)
        super().__init__(label, default, **kwargs)
    def cast(self, value: object) -> str:
        try:
            return super().cast(value)
        except LookupError:
            assert not isinstance(value, Param)
        return str(value)

_Number = TypeVar("_Number", int, float)

class Numeric(ParamDecl, Generic[_Number]): # FIXME: ParamDecl[_Number] ?
    def __init__(self, label: str, default: Optional[_Number],
                 minval: Optional[_Number]=None, maxval: Optional[_Number]=None, **kwargs: Any):
        super().__init__(label, default, **kwargs)
        self.__min: Optional[_Number] = minval
        self.__max: Optional[_Number] = maxval
    def clonable_params(self) -> dict[str,Any]:
        kwargs = super().clonable_params()
        kwargs.update(minval = self.__min, maxval = self.__max)
        return kwargs
    @property
    def min(self) -> Optional[_Number]:
        return self.__min
    @min.setter
    def min(self, value: _Number) -> None:
        if self.__max is not None:
            assert value <= self.__max
        self.__min = value
    @property
    def max(self) -> Optional[_Number]:
        return self.__max
    @max.setter
    def max(self, value: _Number) -> None:
        if self.__min is not None:
            assert value >= self.__min
        self.__max = value
    @property
    def has_min(self) -> bool:
        return self.__min is not None
    @property
    def has_max(self) -> bool:
        return self.__max is not None

class Int(Numeric[int]):
    def __init__(self, label: str, default: int, **kwargs: Any):
        assert isinstance(default, int)
        super().__init__(label, default, **kwargs)
    def cast(self, value: Union[int,Param[int]]) -> int:
        try:
            rawvalue: int = super().cast(value)
            return rawvalue
        except LookupError:
            assert not isinstance(value, Param)
        return int(value)

class Float(Numeric[float]):
    def __init__(self, label: str, default: float, **kwargs: Any):
        assert isinstance(default, float)
        super().__init__(label, default, **kwargs)
    def cast(self, value: Union[float,Param[float]]) -> float:
        try:
            rawvalue: float = super().cast(value)
            return rawvalue
        except LookupError:
            assert not isinstance(value, Param)
        return float(value)

_UserPrintable = TypeVar("_UserPrintable")

class AlternativeBased(ParamDecl[_UserPrintable]):
    def __init__(self, label: str, alternatives: dict[str, _UserPrintable],
                 default: Optional[_UserPrintable]=None, **kwargs: Any):
        super().__init__(label, default=default, **kwargs)
        self.__alternatives = alternatives
    @property
    def alternatives(self) -> dict[str, _UserPrintable]:
        return self.__alternatives
    def clonable_params(self) -> dict[str,Any]:
        kwargs = super().clonable_params()
        kwargs.update(alternatives=dict(self.__alternatives))
        return kwargs

class Choice(AlternativeBased[_UserPrintable]):
    def __init__(self, label: str, alternatives: dict[str, _UserPrintable],
                 default: Optional[_UserPrintable]=None,
                 subparams: Optional[dict[str, dict[str, ParamDecl[_UserPrintable]]]]=None,
                 **kwargs: Any):
        super().__init__(label, alternatives, default=default, **kwargs)
        self.__subparams = subparams or {}
    @property
    def subparams(self) -> Optional[dict[str, dict[str, ParamDecl[_UserPrintable]]]]:
        return self.__subparams
    @property
    def has_subparams(self) -> bool:
        return len(self.__subparams) != 0
    def cast(self, value: Union[_UserPrintable,Param[_UserPrintable]]) -> _UserPrintable:
        try:
            return super().cast(value)
        except LookupError:
            assert not isinstance(value, Param)
        if value not in self.alternatives.values():
            raise LookupError(f'No such choice {value!r}')
        return value
    def default(self) -> Optional[_UserPrintable]:
        declared_default = super().default()
        if  ((declared_default is not None) and
             (declared_default in self.alternatives.values())):
            return declared_default
        if not self.alternatives:
            logger.warning('No alternatives for "%s"', self.label)
            return None
        logger.warning('No sane default (%s) for "%s" (alternatives: %s)',
                       declared_default, self.label, self.alternatives.values())
        return list(self.alternatives.values())[0]

class MultiChoice(AlternativeBased):
    def __init__(self, label: str, alternatives: dict[str, _UserPrintable],
                 default: Optional[Sequence[_UserPrintable]]=None, **kwargs: Any):
        super().__init__(label, alternatives, default=default or [], **kwargs)
    def cast(self, values: Union[Iterable[_UserPrintable],Param[_UserPrintable]]
             ) -> Iterable[_UserPrintable]:
        try:
            return super().cast(values)
        except LookupError:
            assert not isinstance(values, Param)
        for value in values:
            if value not in self.alternatives.values():
                raise LookupError(f'No such choice {value!r}')
        return values
    def default(self) -> list[Optional[ParamValue]]:
        declared_default = super().default() or []
        filtered_default = [v for v in declared_default
                            if v in self.alternatives.values()]
        if filtered_default != declared_default:
            logger.warning(
                'Default (%s) for "%s" has values not in alternatives %s',
                declared_default, self.label, self.alternatives.values())
        return filtered_default

class Plugin(ParamDecl[CorePlugin]):
    def __init__(self, label: str, plugintype: str, context: Context,
                 default_by_name: Optional[str]=None, pythonplugin: bool=True,
                 pattern: plugin_pattern.PluginPattern=plugin_pattern.Any,
                 group_by: Optional[str]=None, **kwargs: Any):
        """A plugin to chose among plugintype, and matching pattern.

        `group_by` can name a class attribute of the Class of
        referenced by a python plugin; it is up to the presentation
        layer to use it to group together plugins whose specified
        class attribute have the same (string) value.
        """
        super().__init__(label, default=None, **kwargs)
        self.__default_by_name = default_by_name
        self.__plugintype = plugintype
        self.__context = context.clone()
        self.__pythonplugin = pythonplugin
        self.__pattern = pattern
        self.__group_by = group_by
    @property
    def plugintype(self) -> str:
        return self.__plugintype
    @property
    def context(self) -> Context:
        return self.__context
    @property
    def pattern(self) -> plugin_pattern.PluginPattern:
        return self.__pattern
    @property
    def group_by(self) -> Optional[str]:
        return self.__group_by
    @property
    def pythonplugin(self) -> bool:
        return self.__pythonplugin

    def default(self) -> Optional[CorePlugin]:
        if self.__default_by_name is not None:
            # FIXME: should also check that this value passes pattern
            # (this is get_plugin's job)
            return self.__context.plugin_manager.get_plugin(self.__plugintype,
                                                            self.__default_by_name,
                                                            pythonplugin=self.__pythonplugin)

        try:
            return self.__context.plugin_manager.find_plugin(self.__plugintype,
                                                             pythonplugin=self.__pythonplugin,
                                                             pattern=self.__pattern)
        except NoSuchPlugin:
            logger.error("found no %s plugin matching pattern %s",
                         self.__plugintype, self.__pattern)
            return None
    def cast(self, value: Union[CorePlugin,Param[CorePlugin]]) -> CorePlugin:
        try:
            rawvalue: CorePlugin = super().cast(value)
            return rawvalue
        except LookupError:
            assert not isinstance(value, Param)
        # FIXME: should check pattern
        if not isinstance(value, CorePlugin):
            raise LookupError(f'No such choice {value!r}')
        return value

    def __pretty__(self, indent: int, curindent: int) -> str:
        return f"Plugin({self.label!r}, {self.plugintype!r})"
